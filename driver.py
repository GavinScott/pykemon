import csv
import json
import sys
import itertools

FNAME = 'pokemon.csv'
TYPES_FNAME = 'types.json'
MAX_PKMN = 30

def load_pokemon():
    pokemon = {}
    with open(FNAME, 'r') as f:
        rows = list(csv.reader(f))
        headers = rows[0]
        name_ndx = headers.index('name')
        for row in rows[1:]:
            pokemon[row[name_ndx].lower()] = \
                {headers[n]:row[n].lower()
                    for n in range(len(headers))}
    return pokemon

def load_types():
    types = {}
    with open(TYPES_FNAME, 'r') as f:
        li = json.load(f)
        for ele in li:
            types[ele['name']] = ele
    return types

def pro_con(pkn, types, name):
    type1 = pkn[name]['type1']
    type2 = pkn[name]['type2']
    s = types[type1]['strengths']
    w = types[type1]['weaknesses']
    if len(type2.strip()) > 0:
        s += types[type2]['strengths']
        w += types[type2]['weaknesses']
    return type1, type2, s, w

def evaluate_team(pkn, types, team, verbose=True):
    weaknesses = []
    strengths = []
    # get relevant key names
    keys = [k for k in pkn[team[0]] if 'against' in k]
    # evaluate
    if verbose:
        print('\nEvaluating:\n')
    for pk in team:
        type1, type2, s, w = pro_con(pkn, types, pk)
        if verbose:
            print('\t', '%10s' % pk.upper(), '\t(Type: {})'
                .format(', '.join([t.title()
                    for t in [type1, type2] if len(t) > 0])))
        strengths.append(s)
        weaknesses.append(w)
    # combine strengths and weaknesses
    weaks = set()
    for ndx in range(len(strengths)):
        maybe = weaknesses[ndx]
        for w in maybe:
            add = True
            if w not in weaks:
                for i in range(len(strengths)):
                    if i != ndx and w in strengths[i] and \
                            w not in weaknesses[i]:
                        add = False
                        break
                if add:
                    weaks.add(w)
    weaks = sorted(list(weaks))
    strongs = []
    for s in strengths:
        strongs += s
    strongs = set(strongs)

    if verbose and len(team) == 1:
        print('\nTeam Strengths:\n')
        print('\t' + '\n\t'.join(
            [w.title() for w in strongs]) + '\n')
    if verbose:
        print('\nTeam Weaknesses:\n')
        print('\t' + '\n\t'.join(
            [w.title() for w in weaks]) + '\n')
    return weaks

def compare_specs(pkn, teams, spec):
    print('\tEvaluating:', spec)
    scores = {}
    max_sc = 0
    for team in teams:
        sc = sum([float(pkn[p][spec]) for p in team])
        scores[team] = sc
        max_sc = max(max_sc, sc)
    return set([t for t in teams if scores[t] == max_sc])

def find_best_team(pkn, types, options, args=[],
        weak_types=None, verbose=True):
    # find all possible weaknesses
    weaknesses = {}
    min_weaks = 100
    n = 1
    for team in itertools.combinations(options, 6):
        if n % 1000000 == 0:
            print('\tProcessed {} teams...'.format(n))
        n += 1
        weaks = evaluate_team(pkn, types, team, verbose=False)
        if weak_types != None:
            weaks = [w for w in weaks if w in weak_types]
        min_weaks = min(min_weaks, len(weaks))
        if len(weaks) <= min_weaks:
            weaknesses[team] = weaks
    print('Processed {} teams'.format(n))
    # filter out too many weaknesses
    winners = set([t for t in weaknesses
        if len(weaknesses[t]) == min_weaks])

    # other args
    arg_ndx = 0
    while len(winners) > 1 and arg_ndx < len(args):
        winners = compare_specs(pkn, winners, args[arg_ndx])
        arg_ndx += 1

    if verbose:
        print('\nWinning Team(s):')
        for team in winners:
            print('\t', ', '.join(sorted([p.title() for p in team])))
            print('\t\tWeaknesses:', ', '.join(
                [w.title() for w in weaknesses[team]]))
    return winners, weaknesses

def evaluate_opponent(pkn, types, options, opponent):
    print()
    # minimize weaknesses
    opp_strengths = []
    opp_weaknesses = []
    for opp in opponent:
        type1, type2, s, w = pro_con(pkn, types, opp)
        opp_strengths.extend(s)
        opp_weaknesses.extend(w)
    teams, weaknesses = find_best_team(pkn, types, options,
        weak_types=set(opp_strengths),
        verbose=False)

    # maximize strengths
    team_strengths = {}
    for t in teams:
        # get strengths
        strengths = []
        for pk in t:
            type1, type2, s, w = pro_con(pkn, types, pk)
            strengths.extend(s)
        team_strengths[t] = [s for s in opp_weaknesses
            if s in set(strengths)]
    best = max([len(l) for l in list(team_strengths.values())])
    teams = [t for t in teams if len(team_strengths[t]) == best]

    if len(teams) > 1:
        teams = compare_specs(pkn, teams, 'base_total')

    print('\nBest team(s):')
    for t in teams:
        print('\t', ', '.join(sorted([p.title() for p in t])))
        print('\t\t Weaknesses:',
            ', '.join([w.title() for w in weaknesses[t]]))
        print('\t\t Strengths:',
            ', '.join([s.title() for s in set(team_strengths[t])]))

if __name__ == '__main__':
    pkn = load_pokemon()
    types = load_types()

    poss_args = [
        '-attack',
        '-defense',
        '-sp_attack',
        '-sp_defense',
        '-hp',
        '-speed',
        '-is_legendary',
        '-base_happiness',
        '-base_total',
        '-against_[TYPE] (-against_fight, -against_rock, etc.)'
    ]

    if '-help' in sys.argv or '--h' in sys.argv:
        print('Usage: python3 driver.py PKMN1, 2...  -arg1 -arg2..')
        print('\tIf 6 or fewer PKMN, it shows team weaknesses.')
        print('\tIf more than 6 PKMN, finds the best team(s)')
        print('\t\tFor finding best team:')
        print('\t\t\t1. Looks for fewest possible weaknesses')
        print('\t\t\t2. Compare args (in order) until one team')
        print('\t\t\t\tPossible args:')
        for key in poss_args:
            print('\t\t\t\t\t' + key)
        exit()

    if '-vs' in sys.argv:
        options = [a for a in sys.argv[1:sys.argv.index('-vs')]]
        opponent = [a for a in sys.argv[sys.argv.index('-vs') + 1:]]
        evaluate_opponent(pkn, types, options, opponent)
    else:
        inp = list(set([a.lower() for a in sys.argv[1:]
            if '-' not in a]))
        if len(inp) > MAX_PKMN:
            print('Too many pokemon! Max:', MAX_PKMN)
            exit()
        args = list([a.lower()[1:] for a in sys.argv[1:]
            if '-' in a])

        if len(inp) <= 6:
            evaluate_team(pkn, types, inp)
        else:
            find_best_team(pkn, types, inp, args)